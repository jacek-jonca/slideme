# Install slideme
This instruction only apply in Linux, and had been test in Ubuntu 16.04  

#Dependencies
- Ruby on Rails 
- wkhtmltoimage

# wkhtmltoimage
This tools are use for convert HTML to image for create thumbnail to slide (deck) 
```sh
    sudo apt install wkhtmltopdf
```
Download [wkhtmltox-0.12.3_linux-generic-amd64.tar.xz](http://download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz)  
```sh
    wget http://download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
    tar -xzvf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
    cd wkhtmltox
    sudo cp wkhtmltoimage /usr/bin/wkhtmltoimage
```
# My Notes
Ruby 2.38
```sh
gem uninstall bundler -v 2.1.4
gem install bundler -v 1.17.3
```

#might help

```sh
gem pristine bcrypt --version 3.1.11
gem pristine binding_of_caller --version 0.7.2
gem pristine byebug --version 8.2.5
gem pristine debug_inspector --version 0.0.2
gem pristine executable-hooks --version 1.6.0
gem pristine gem-wrappers --version 1.4.0
gem pristine json --version 1.8.6
gem pristine kgio --version 2.10.0
gem pristine mysql2 --version 0.4.4
gem pristine nokogiri --version 1.6.7.2
gem pristine raindrops --version 0.17.0
gem pristine sqlite3 --version 1.3.11
gem pristine unicorn --version 5.1.0
```

# Install dependencies
```sh
    gem install bundle
    bundle install
    #Install bower
    sudo apt-get install nodejs-legacy npm
    sudo npm install bower -g
    bower install
```

# Create configurations
Create file for database configuration
```sh
    touch config/database.yml
```

Set your database values depending on environment
```yml
default: &default
  adapter: sqlite3
  pool: 5
  timeout: 5000

development:
  <<: *default
  database: db/development.sqlite3

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: db/test.sqlite3

production:
  adapter: mysql2
  database: slideme
  host: localhost
  username: root
  password: pass
  encoding: utf8
```
# Development
```sh
    rails server
```
Open web browser with url **http://localhost:300** 

# Production 
To Do 
