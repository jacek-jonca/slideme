See INSTALL.md

#Dependencies
- Rails 4.2.6
- Ruby 2.3.8
- gem uninstall bundler -v 2.1.4
- gem install bundler -v 1.17.3
- wkhtmltoimage

#Deploy with capistrano and unicorn 
cap production deploy

### Up Unicorn
bundle exec unicorn -c /var/www/slideme/current/config/unicorn/production.rb -E deployment -D

#Helpful
https://www.codewithjason.com/how-to-deploy-a-ruby-on-rails-application-to-aws/
